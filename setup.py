import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()
    
setuptools.setup(
    name="plclink",
    version="0.3.7",
    author="Tomasz Hasinski",
    author_email="tomasz.hasinski@prohas.pl",
    description="Read/Write PLC's",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="Apache License 2.0",
    url="https://gitlab.com/hasz/plclink",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
        ],
    )
